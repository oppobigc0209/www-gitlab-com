---
layout: handbook-page-toc
title: FY22-Q1 Learning & Development Newsletter
---

Welcome to the GitLab Learning & Development (L&D) newsletter! The purpose of the L&D newsletter is to enable a culture of curiosity and continuous learning that *prioritizes learning* with a [growth mindset](/handbook/values/#growth-mindset) for team members. The quarterly newsletter will raise awareness of what learning initiatives took place in the past quarter, insight into what's coming next, learning tips, and encourage participation. We will also feature leadership and learner profiles that highlight what our community has done to learn new skills. Consider this a forum to hear from others across GitLab on what learning has done for them. 

You can find more information on the [structure and process](/handbook/people-group/learning-and-development/newsletter/) for the L&D newsletter, as well as links to [past L&D Newsletters](/handbook/people-group/learning-and-development/newsletter/#past-newsletters). 

## Learn from Leadership 

This quarter we are learning from [Robin Schulman](/company/team/#rschulman). 

<figure class="video_container">
  <iframe width="560" height="315" src="https://docs.google.com/presentation/d/e/2PACX-1vTkjuhFvtlpq4XkZWRFcHWuCW3TpVccNHW7OC7vGCiDZm8u7g3TgaZipsfuRK74rs0ODFS4k3lTOxiD/embed?start=false&loop=false&delayms=3000" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

**Q: What has your career journey looked like to date?**

- _Robin_: I graduated from NYU film school, and worked for several years in the film industry and then in technology before deciding to go to law school. I have always been drawn towards technology, having grown up in a house with every Apple computer but the Lisa.
- _Robin_: After receiving my law degree I joined a Bay Area law firm that specializes in working  with technology clients. After several years of working closely with clients in a variety of technology fields, I was recruited to Adobe’s legal team, and then went on to join New Relic as their General Counsel. The intersection of law and technology continues to fascinate me. I especially enjoy the challenge of my role at GitLab, given the speed of constant innovation and the juxtaposition against traditional legal constructs. 
- _Robin_: I’m drawn to GitLab’s Values, especially our transparency, which I believe dovetails with many of the reasons behind various legal structures and requirements. 

**Q: What’s the biggest career risk you’ve ever taken?**

- _Robin_: The biggest career risk I have taken was deciding to change careers and go to law school.

**Q: What’s the best book you’ve read in the past year or favorite book in general?**

- _Robin_: I am currently reading “[Undaunted: Overcoming Doubt and Doubters](https://www.amazon.com/Undaunted-Overcoming-Doubters-Kara-Goldin/dp/1400220289)” by Kara Goldin, the CEO of Hint Water. 

**Q: What is your preferred learning style? (Different learning styles - Visual: You prefer using pictures, images, and spatial understanding. Auditory: You prefer using sound and music. Kinesthetic: You prefer using your body, hands and sense of touch. Reading/Writing: You prefer using words, both in speech and writing.)**

- _Robin_: I use a combination of all learning styles. I find that I remember things best when I write them down.
 
**Q: Why do you think learning is important?**

- _Robin_: I think learning is important because we work in technology where things are constantly evolving. There is a boundless need for information and learning. I’m not sure you could name an industry where technology isn’t involved. I especially love learning about how our customers are using our product to innovate in their own fields.  

**Q: What is one thing you wish you would have learned sooner in your career?**

- _Robin_: That you are not going to know all the answers off the top of your head all of the time and that it’s okay not to know as long as you ask questions to arrive at the right result and know where to find the answer. If your gut is telling you something or urging you to ask the question, you are probably onto something. It’s better to speak up than to wait until things are too far down the line.

**Q: Can you name a person who has had a tremendous impact on you as a leader? Maybe someone who has been a mentor to you. Why and how did this person impact your life.** 

- _Robin_: I am deeply influenced by the former General Counsel at Adobe. Early on, he told me I would one day be a successful General Counsel/Chief Legal Officer and gave me a lot of encouragement and valuable counsel over the years. He loved hearing about my adventures at a young, quickly growing public company and was never further away than a quick text. My role as a CLO can be a lonely one so having someone who has “been there, done that” was invaluable to my career evolution. So much so that I make it a priority to provide the same for anyone who is in a similar place on their career trajectory. 
- _Robin_: Also, my husband. Years ago when I was deciding whether to accept my first General Counsel role, he asked me how I would feel if I turned it down and then I heard about the person who ended up taking the job. It made my choice incredibly clear :-) 
- _Robin_: People are the most impactful when they push themselves beyond the familiar and comfortable. He has always supported me and that has allowed me to feel secure in expanding my comfort zone. 

**Q: What are you doing to ensure you continue to grow and develop as a leader?**

- _Robin_: Listen. I learn from the people I collaborate with every day - both in and outside of GitLab. It’s not about telling people how or what to be. It’s listening and trying to help them strategically problem-solve.
- _Robin_: As an organization grows and scales, it’s imperative not to outsource your understanding. While delegation is necessary for a company to scale, I believe an effective leader can delegate successfully while remaining aware of and close to the issues that people are working on.

**Q: Is there anything else you would like to tell us about Leadership at GitLab?**

- _Robin_: Leadership is a mindset, not a title. Individuals can be leaders. Here at GitLab, people can lead in their subject matter expertise, or on certain projects as DRIs or in the activities they are involved in (i.e. TMRGs). If leadership is something that interests you but is not part of your day-to-day role, look for opportunities outside of that. 
- _Robin_: Ask for help when you need it. Too often people won’t ask for help because they view it as a weakness. 
- _Robin_: Two things that have been key at GitLab for me are: the lack of political culture here and transparency so people understand how decisions are made and arrived at.  

**Fun Facts about Robin**

- You can find me in my favorite slack channel: `#dog` 
- In my free time, I love spending time with my family and baking. I made homemade bagels for the first time last year. 

**Is there a leader at GitLab that you want to learn more about?** To nominate someone for our Learn from Leadership section in our next newsletter, use [this nomination form](https://docs.google.com/forms/d/e/1FAIpQLSeuOIH2r_gaQlv6woW96_8BfjBUbzWxLuxoZA7TW-MXz7cT0g/viewform). 

## Learner Spotlight   

Our Learner Spotlight for this quarter is [John Long](https://about.gitlab.com/company/team/#john_long). We would like to thank [Aric Buerer](https://about.gitlab.com/company/team/#abuerer) for nominating John! Continue reading to learn more about John and how he prioritizes learning.  

* **What is your current role at GitLab?** 
   * _John_: I am a Senior Support Engineer.
* **How long have you been a GitLab team member?** 
   * _John_: I have been a GitLab team member for 1 year and 1 month.
* **How do you make time for learning in your normal day-to-day?** 
   * _John_: In order to make time for learning, I block time off in my calendar. This allows me to have free time that I can use to complete courses on various learning platforms.
* **Why do you make learning a priority?** 
   * _John_: I do not currently see myself in the position I want to retire in. In order for me to get to where I want to be, I have to focus on learning. Learning new skills is not just to get ahead in my current position, but to prepare for my next.
* **What is your preferred learning style? (Different learning styles - Visual: You prefer using pictures, images, and spatial understanding. Auditory: You prefer using sound and music. Kinesthetic: You prefer using your body, hands and sense of touch. Reading/Writing: You prefer using words, both in speech and writing.)** 
   * _John_: I learn best by seeing - Visual - and doing - Kinesthetic. I prefer to watch how-to videos with examples of how to do something. This allows me to process and figure out how it works.
* **Have you recently completed a course? If so, which course (or courses) did you complete and why?** 
   * _John_: Within a few hours of getting the LinkedIn Learning license, I began to take advantage of this new learning avenue.
I started with [Quality Standards in Customer Service](https://www.linkedin.com/learning/quality-standards-in-customer-service-4/quality-matters-in-customer-service). I am interested in moving into management at some point in my career and feel taking courses geared towards success in customer service, as well as courses in management, will help me get to my goal. LinkedIn Learning offers plentiful resources for this career track.
   * _John_: Next, I completed [Ruby Essential Training Part 1: The Basics](https://www.linkedin.com/learning/ruby-essential-training-part-1-the-basics/learn-the-basics-of-ruby). I am decently familiar with Ruby at this point, but I am still mostly self taught. I thought I would take a more introductory course to fill in any missing pieces and help take my skills to the next level.
* **Do you have a favorite or recommended course for others?** 
   * _John_: The absolute best course I have taken thus far in my career was Ansible 101. This course is expensive, and I skipped the certification as that adds even more cost, but the content and design of the course was engaging. 
* **What advice would you give someone who is struggling to make learning a priority?** 
   * _John_: Try and schedule the time. Blocking off the calendar can help make time for learning. Try and find something engaging and interesting to you. It can also be helpful to take a course with a friend or co-worker.
* **Anything else you would like to share about learning?**
   * _John_: Learning is one of the most important things we do. It doesn’t have to be work related. Everything we know we have learned. There is a lot left to learn. Our whole lives are spent learning. Learning can be fun, but can sometimes be boring. Always try to make the best of it and get the most out of it that you can. Sometimes you learn something that you take for granted and do not give the course the attention it deserves. Then when you need the knowledge or skills from that course, it’s not there. Sometimes there are courses that we must take that may seem boring, uninteresting, or unrelated to us specifically. But, having the skills or knowledge from that course could make a big impact on someone else in the future.

**Do you know a team member that places an emphasis on learning?** To nominate someone for our Learner Spotlight for our next newsletter, use [this nomination form](https://docs.google.com/forms/d/e/1FAIpQLSfi72ONbp8UcUXDCL__TPAoCEEGH4K_9i1-ZQN7yh_YzlVx0w/viewform). 

## Department Spotlight 

This quarter's team spotlight is on the [Diversity, Inclusion, & Belonging](/company/culture/inclusion/) (DIB) Team. The DIB Team has two team members that focus on building an environment where all team members feel they belong. They do this by making sure team members feel: they are recognized, allowed to express their thoughts, have the ability to make contributions and are able to bring their best selves to work every day. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://docs.google.com/presentation/d/e/2PACX-1vQaZj9jgdFsujXcEcSPh0G5fLh67uiqfZ7NOaGk3rQBwnz-aVtP4HNe7lbIh9fnZO-0rlsDfbNdTDPD/embed?start=false&loop=false&delayms=3000" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

The DIB Team has worked on many projects to date, including, but not limited to: 

* Live Learning Sessions:  
   * [Being Inclusive](/company/culture/inclusion/being-inclusive/#inclusion-training)
   * [Being an Ally](/company/culture/inclusion/being-an-ally/#ally-training)
   * [Recognizing Bias](/company/culture/inclusion/unconscious-bias/#recognizing-bias-training)
   * [Belonging](https://www.youtube.com/watch?v=WZun1ktIQiw)
* Diversity, Inclusion, and Belonging Certification & Training
   * [Handbook Page](/company/culture/inclusion/dib-training/)

**Is there a team at GitLab you want to learn more about?** Each quarter we will feature a different team and what they do here at GitLab. Leave a comment on the [dicussion issue](https://gitlab.com/gitlab-com/people-group/learning-development/newsletter/-/issues/8) if there is a team you would like to see featured or if you would like your team to be featured! 

## Upcoming in FY22-Q1 

Our L&D team will have a variety of learning initiatives throughout the quarter to help reinforce our culture of learning. 

* Continuation of [support for managing burnout](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/24)
* [Mentorship opportunities at GitLab](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/16)
* New resource coming soon: [Tuition reimbursement suggested program list](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/163)
* GitLab Learn BINGO 
   * Anyone is welcome to join our upcoming Learning BINGO challenge to become more familiar with GitLab Learn and LinkedIn Learning. The challenge will last two weeks where you will have the opportunity to complete items to get a "BINGO" and earn a new badge on GitLab Learn!
* Another Manager Challenge planning to launch in March
* [CEO Handbook Learning Sessions](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions)
* Crucial Conversations Certification will be completed by L&D Team Members to be able to provide training across the company later in the FY22! 
* We will be transitioning from [Monthly Continuous Learning Calls](/handbook/people-group/learning-and-development/learning-initiatives/#monthly-continuous-learning-call-overview) to a Learning & Development Group Conversation. Our first Group Conversation will be 2021-04-13!  
* GitLab Learn launch to external audiences!

**Note:** More learning activities may be added throughout the quarter. To stay up to date, please join our [#learninganddevelopment](https://app.slack.com/client/T02592416/CMRAWQ97W) Slack Channel. 

## Recap of FY21-Q4

FY21-Q4 was full of Learning Initiatives by the Learning & Development Team. If you missed them or want to go back and review anything, here is an outline of what took place: 

* 2020-11-10: Live Learning - [Belonging](/company/culture/inclusion/#gitlabs-definition-of-diversity-inclusion--belonging)
* 2020-11-12: Learning Speaker Series - [Building Trust](/handbook/leadership/building-trust/#learning-speaker-series---building-trust-with-remote-teams) 
* 2020-11-16 - 2020-11-20 - [Psychological Safety - One Week Challenge](/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge)
* 2020-12-03: Live Learning - [Introduction to Coaching](/handbook/leadership/coaching/) 
* 2020-12-07 - 2020-12-11 - Mental Health Awareness Campaign  
   * [Mental Health Awareness Week Recap](https://about.gitlab.com/blog/2020/12/21/gitlab-mental-health-awareness-week-recap/)
   * 2020-12-10: Learning Speaker Series - [Managing Mental Health & Burnout](https://about.gitlab.com/company/culture/all-remote/mental-health/#rest-and-time-off-are-productive)
   * [Mental Health Awareness Newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY21-Q4/) 
* 2020-12-14 Launched a new Learning Initiative - [CEO Handbook Learning Sessions](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions) 
* 2021-01 - [Manager Challenge](/handbook/people-group/learning-and-development/manager-challenge/) - currently in progress
* 2021-01-11 - GitLab Learn Internal Launch
* 2021-01-11 - LinkedIn Learning Launch 
* [Monthly Continuous Learning Calls](/handbook/people-group/learning-and-development/learning-initiatives/#past-monthly-continuous-learning-call)
   * [2020-11-18](https://www.youtube.com/watch?v=tWZ3iVyb-4E&feature=youtu.be)
   * [2020-12-16](https://www.youtube.com/watch?v=DQdzXFu9Nbc) 
   * [2021-01-20](https://www.youtube.com/watch?v=pnc4FqHk_a0)

### New and Updated Leadership Pages 

The L&D Team has been adding more content to the leadership handbook pages and doing [CEO Handbook Learning Sessions](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions). You can see an outline of the leadership pages below. 

* [Building Trust](/handbook/leadership/building-trust/)
* [Coaching](/handbook/leadership/coaching/)
* [Compensation Review Conversations](/handbook/leadership/compensation-review-conversations/)
* [Crucial Converations](/handbook/leadership/crucial-conversations/)
* [Effective Delegation](/handbook/leadership/effective-delegation/)
* [Emotional Intelligence](/handbook/leadership/emotional-intelligence/)
   * [Psychological Safety](/handbook/leadership/emotional-intelligence/psychological-safety/)
   * [Social Styles](/handbook/leadership/emotional-intelligence/social-styles/)
* [High Output Management](/handbook/leadership/high-output-management/)
* [High Performing Teams](/handbook/leadership/build-high-performing-teams/) 
* [Making Decisions](/handbook/leadership/making-decisions/)
* [Managing Conflict](/handbook/leadership/managing-conflict/) 
   * [Leading through Adversity](/handbook/leadership/managing-conflict/leading-through-adversity/)
* [No Matrix Organization](/handbook/leadership/no-matrix-organization/)
* [Underperformance](/handbook/leadership/no-matrix-organization/) (moved to be a sub-page of Leadership)
* [Workforce Planning](/handbook/leadership/workforce-planning/)

The following video is an example of the [CEO Handbook Learning Sessions](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions) the L&D Team is doing to uplevel the Leadership handbook pages. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/_okcPC9YucA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Learning & Development Blog Posts 

* [GitLab Mental Health Awareness Recap](https://about.gitlab.com/blog/2020/12/21/gitlab-mental-health-awareness-week-recap/) 
* [Building a Handbook First Remote Learning Culture](https://about.gitlab.com/blog/2020/12/22/building-a-handbook-first-remote-learning-culture/)

## Learning Spotlight 

More ways you can continue to learn throughout the quarter: 

* Access a LinkedIn Learning Licenses. We have outlined a [variety of different courses](/handbook/people-group/learning-and-development/linkedin-learning/) that you can take. 

## Learning Tips 

We want to remind you to [take time out to learn](/handbook/people-group/learning-and-development/#take-time-out-to-learn-campaign)! [Focus Fridays](/handbook/communication/#focus-fridays) have been extended to 2021-04-30 and are a great time to focus on learning. Consider blocking off a few hours each week on Fridays to learn new skills for your role at GitLab. 

There are plenty of courses on LinkedIn Learning that you can access. Check out our [recommended LinkedIn Learning courses](/handbook/people-group/learning-and-development/linkedin-learning/)! If you find a course that you think should be on the recommendation list, make an MR to the list and ask a L&D Team member in the `#learninganddevelopment` Slack Channel to merge. 

## Other Enablement Initiatives

* Check out the monthly [Field Flash Newsletter](/handbook/sales/field-communications/field-flash-newsletter/#past-newsletters)
* The Diversity, Inclusion, & Belonging Team has a newsletter. Search your inbox for the subject line `DIB - Diversity, Inclusion and Belonging Quarterly Newsletter`. 

## Discussion 

If you would like to discuss items related to this newsletter, please see the [related discussion issue](https://gitlab.com/gitlab-com/people-group/learning-development/newsletter/-/issues/8).
