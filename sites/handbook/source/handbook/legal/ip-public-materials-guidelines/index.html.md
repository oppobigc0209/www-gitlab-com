---
layout: handbook-page-toc
title: "Guidelines for Use of Third-party IP in External Materials"
description: "Guidelines for the use of third-party content in external materials"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
 
## Guidelines for Use of Third-party IP in External Materials


### Scope and purpose

These guidelines apply to the use of third-party content in all external materials, including:



* Pitch decks;
* Presentations and talk tracks (including those delivered at conferences and marketing events);
* Marketing collateral, including emails, blog posts, release posts, social media posts on GitLab accounts, and videos; and 
* Advertisements.

If you’re unsure, reach out to [#legal](https://app.slack.com/client/T02592416/C78E74A6L) to confirm.

These guidelines do not apply to the use of trademarks in the GitLab product itself. Refer to the guidelines of the [Use of Third-party Trademarks in GitLab](https://about.gitlab.com/handbook/legal/policies/product-third-party-trademarks-guidelines/#dos--donts-for-use-of-third-party-trademarks-in-gitlab) instead.

When used in these guidelines:

“**external materials**” means materials made available to any company, or individual who is not a GitLab team member, including marketing audiences, prospects, customers, partners, vendors, community members, and conference attendees. Materials published [with public visibility](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#visibility) on the GitLab Unfiltered YouTube channel, or on any GitLab social media account, are public-facing.

“**third-party content**” means any content, including trademarks, visual content, and written content, created or owned by a third party.


### Note about use of content obtained from the internet

Just because something is available for free on the internet (including on Google Images, Google Maps, YouTube, blogs, social media, and news websites) does not mean it can be freely used. The vast majority of internet content is subject to copyright and/or trademark rights, and GitLab’s use of that content could constitute infringement.


### **Trademarks** - logos and wordmarks

“**Logo**” means a symbol used to identify a company, product, or service, like the [GitLab Tanuki logo](https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png).

“**Wordmark**” means unstylized words or letters used to identify a company, product, or service, like _GitLab_.


#### Do



* When using the **logo** or **wordmark** of a customer, partner, or other third party with which GitLab has a commercial relationship, refer to the [Customer Reference Program handbook page](https://about.gitlab.com/handbook/marketing/strategic-marketing/customer-reference-program/). Discuss with [#customer_references](https://app.slack.com/client/T02592416/CLFCPMF8E) if unsure.
* Use **wordmarks** referentially: refer to the wordmark owner, or the owner’s products and services associated with the mark, when the company, product or service in question cannot be easily identified without using the mark. For example, it’s much easier to refer to _GitLab_ using the wordmark _GitLab_ than to _the company behind the DevOps platform delivered as a single application_.


#### Don’t



* Use a **logo** when referring to third-party companies, or their products or services. Trademark fair use applies to logos only in very exceptional circumstances; use the wordmark instead.
* Remove, distort, alter, or otherwise modify a **logo**.
* Use a **logo** or **wordmark** in any way that implies an untrue relationship or affiliation with, or sponsorship or endorsement by, the owner.
* Use a **logo** or **wordmark** in any way that denigrates or discredits the owner or their products or services.


### **Visual content**, including videos, gifs, photographs, illustrations, graphics, and artwork


#### Do



* Obtain content from permissively-licensed sources, like [unsplash.com*](https://unsplash.com/) for images, or use content created by GitLab.
* If you’re unsure, reach out to [#legal](https://app.slack.com/client/T02592416/C78E74A6L) to review the license covering the content you want to use.
* Provide attribution, ensuring that you follow any attribution guidelines which apply to the content you are using.

*For Unsplash images, it’s good practice to credit Unsplash as the source and photographer. This helps team members repurposing materials determine if images can be used, and is courteous to the photographer.

{::options parse_block_html="true" /}
<details>
<summary markdown="span">Expand for Unsplash attribution instructions</summary>

1. Create a small text box at the bottom right of the relevant slide.
2. Locate the image on [unsplash.com](https://unsplash.com).
3. Click on `Download free` but ignore the downloaded file.
4. Copy the `Say thanks 🙌` attribution text to the clipboard.
5. Insert the attribution text into the text box, and apply formatting: Font `Inter`, Size `8`, `Italic`.

[Example Unsplash attribution](https://docs.google.com/presentation/d/1k4PWKJR9O1jEGxKblSQtjDsloQ95uvu6Ty9Pjpmin7E/edit#slide=id.g129bb425d32_0_273)
</details>
{::options parse_block_html="false" /}

#### Don’t



* Use images depicting people (including GitLab team members), unless a release for use of their likeness is in place.
* Use images where event attendee name badges are visible, as we should avoid identifying people by name. For example, blur the name badge, where possible, or delete the photo.
* Use images depicting children.
* Use images which prominently feature a logo, famous building or structure, or well-known person.


### **Written content**, including books, papers, blog posts, and news articles


#### Do



* Use excerpts from written content for the purpose of criticism or comment.
* Use as short an excerpt of the original content as possible for your given purpose. Fair use is more likely to be established when a small portion of the original content is copied.
* Provide attribution, ensuring that you follow any attribution guidelines which apply to the content you’re using.


#### Don’t



* Use the entirety, or a large portion, of an article, paper or blog post.
* Use excerpts, quotes or headlines of print and online news articles without following Corporate Communications’ [Sharing Media Coverage guidelines](/handbook/marketing/corporate-marketing/corporate-communications/#sharing-media-coverage).
