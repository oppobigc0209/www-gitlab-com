---
layout: markdown_page
title: "GitLab Promotional Games"
description: "View a full listing of current and previous GitLab Promotional Games. Find more information here!"
canonical_path: "/community/sweepstakes/"
---
<!-- Housekeeping: Please deprecate any Promotional Game drawn more than 180 days ago. -->
## Current and previous promotional games

### Current
- [KubeCon EU 22 GitLab Product Direction Survey Prize Draw](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/tree/master/-/sites/uncategorized/source/community/sweepstakes/KubeCon_EU_22_GitLab_Product_Direction_Survey/index.html.md/)
- [SCale 19x 2022 GitLab Newsletter Signup and Sweepstakes - Official Rules](/community/sweepstakes/scale-19x-newsletter-sweepstakes/)
- [SCale 19x 2022 GitLab Code Challenge - Official Rules](/community/sweepstakes/scale-19x-code-challenge/)

### Past
- [GitLab Survey weekly digest 2022](/community/sweepstakes/2022-survey-weekly-digest/survey-weekly-digest.index.html)
- [GitLab IT Revolution iPad Sweepstakes](https://about.gitlab.com/community/sweepstakes/gitlab-it-revolution-ipad-sweepstakes/)
- [Open Source Summit 2022 Code Challenge - Official Rules](/community/sweepstakes/ossna-code-challenge/)
- [GitLab Product Survey Sweepstakes - Official Rules](https://about.gitlab.com/community/sweepstakes/Product_Survey_Sweepstakes/product-survey.index.html)
